import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavComponent } from './components/nav/nav.component';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [DashboardComponent, NavComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatIconModule,
    RouterModule.forChild([
      {
        path: '',
        component: DashboardComponent,
        children: [
          { path: 'courses', loadChildren: () => import('../courses/courses.module').then(m => m.CoursesModule)},
          { path: 'reminders', loadChildren: () => import('../reminders/reminders.module').then(m => m.RemindersModule)},
          { path: 'both', loadChildren: () => import('../both/both.module').then(m => m.BothModule)},
        ],
      },
    ]),
  ],
  exports: [DashboardComponent, NavComponent]
})
export class CoreModule { }
