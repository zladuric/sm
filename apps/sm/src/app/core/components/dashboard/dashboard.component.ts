import { Component } from '@angular/core';

@Component({
  selector: 'sm-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent  {
  date = new Date().toLocaleString();
  completion = '80';
}
