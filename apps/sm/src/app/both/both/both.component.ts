import { Component, OnInit } from '@angular/core';
import { Course, Reminder } from '../../services/models';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../store/reducers';
import { Observable } from 'rxjs';
import { decrement, increment, flip, coursesLoaded } from '../../store/actions';
import { coursesLoadingSelector, coursesSelector } from '../../store/selectors';

@Component({
  selector: 'sm-both',
  templateUrl: './both.component.html',
  styleUrls: ['./both.component.scss']
})
export class BothComponent implements OnInit {
  reminders: Reminder[] = [];
  remindersLoading = false;

  courses$: Observable<Course[]>;
  coursesLoading$: Observable<boolean>;

  constructor(private store: Store<any>) {
    this.courses$ = store.pipe(coursesSelector);
    this.coursesLoading$ = store.pipe(coursesLoadingSelector);
  }

  ngOnInit(): void {
  }

  inc(id: string) {
    this.store.dispatch(increment({ payload: { id } }));
    this.store.dispatch({
      type: 'One for Z.'
    })
  }

  dec(id: string) {
    this.store.dispatch(decrement({ payload: { id }}))
  }

  flip(id: string) {
    this.store.dispatch(flip({ payload: { id }}))
  }
}
