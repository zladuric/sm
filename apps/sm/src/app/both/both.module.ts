import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BothComponent } from './both/both.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { WidgetsModule } from '../widgets/widgets.module';


@NgModule({
  declarations: [BothComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: BothComponent }]),
    MatIconModule,
    WidgetsModule,
  ],
})
export class BothModule {
}
