import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { delay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Course } from './models';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {

  constructor(private http: HttpClient) {
  }

  getCourses(): Observable<Course[]> {
    return this.http.get<Course[]>('/assets/data/courses.json')
      .pipe(
        delay(200 + Math.floor(Math.random() * 1500)),
      );
  }
}
