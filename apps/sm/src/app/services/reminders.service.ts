import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Reminder } from './models';

@Injectable({
  providedIn: 'root'
})
export class RemindersService {

  constructor(private http: HttpClient) { }

  getReminders(): Observable<Reminder[]> {

    return this.http.get<unknown[]>('/assets/data/reminders.json')
      .pipe(
        map(rawReminders => rawReminders.map((reminder: unknown) => ({
            // @ts-ignore
            ...reminder,
            date: new Date(reminder.date)
          }))
        ),
        delay(200 + Math.floor(Math.random() * 1500)),
      )
  }
}
