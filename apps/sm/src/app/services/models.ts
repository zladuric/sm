export type ReminderType = 'alarm' | 'email';

export interface Reminder {
  id: string;
  type: ReminderType;
  name: string;
  date: Date;
  contact: string;
  done: boolean;
}
export interface Course {
  id: string;
  short: string;
  name: string;
  description: string;
  completed: number;
  extras: {
    vocab: number,
    grammar: number;
  };
}
