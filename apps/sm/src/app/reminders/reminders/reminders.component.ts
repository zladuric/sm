import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../../services/reminders.service';
import { Reminder } from '../../services/models';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sm-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.scss']
})
export class RemindersComponent implements OnInit {
  reminders: Reminder[] = [];
  loading = false;

  constructor(private remindersService: RemindersService) { }

  ngOnInit(): void {
    this.loading = true;
    this.remindersService.getReminders()
      .pipe(finalize(() => this.loading = false))
      .subscribe(reminders => this.reminders = reminders);
  }

  flip(id: string) {
    const reminder = this.reminders.find(r => r.id === id);
    reminder.done = !reminder.done;
  }
}
