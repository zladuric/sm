import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RemindersComponent } from './reminders/reminders.component';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { WidgetsModule } from '../widgets/widgets.module';


@NgModule({
  declarations: [RemindersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: RemindersComponent }]),
    MatIconModule,
    WidgetsModule,
  ],
})
export class RemindersModule {
}
