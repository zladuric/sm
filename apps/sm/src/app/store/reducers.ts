import { Course, Reminder } from '../services/models';
import { createReducer, on } from '@ngrx/store';
import { coursesLoaded, decrement, flip, increment, loadCourses, loadReminders, remindersLoaded } from './actions';

export interface AppState {
  courses: Course[];
  reminders: Reminder[];
  coursesLoading: boolean;
  remindersLoading: boolean;
}

const initialState = { courses: [], reminders: [], coursesLoading: false, remindersLoading: false };

const incrementCourse = (state: AppState, id: string) => {
  const courses = state.courses.map(
    course => course.id !== id ? course :
      {
        ...course,
        completed: course.completed + 1,
      }
  );
  return {
    ...state,
    courses
  };
};
const decrementCourse = (state: AppState, id: string) => {
  const courses = state.courses.map(
    course => course.id !== id ? course :
      {
        ...course,
        completed: course.completed - 1,
      }
  );
  return {
    ...state,
    courses
  };
};

const flipReminder = (state: AppState, id: string) => {
  const reminders = state.reminders.map(
    reminder => reminder.id !== id ? reminder :
      {
        ...reminder,
        done: !reminder.done
      }
  );
  return {
    ...state,
    reminders
  };
};

const handleLoadCourses = (state) => {
  return {
    ...state,
    courses: [],
    coursesLoading: true
  }
}

const handleCoursesLoaded = (state, courses) => ({
  ...state,
  courses,
  coursesLoading: false
});

const handleLoadReminders = (state) => ({
  ...state,
  reminders: [],
  remindersLoading: true
});

const handleRemindersLoaded = (state, reminders) => ({
  ...state,
  reminders,
  remindersLoading: false
});
const appReducer = createReducer(initialState,
  on(increment, (state, { payload: { id } }) => incrementCourse(state, id)),
  on(decrement, (state, { payload: { id } }) => decrementCourse(state, id)),
  on(flip, (state, { payload: { id } }) => flipReminder(state, id)),
  on(loadCourses, state => handleLoadCourses(state)),
  on(coursesLoaded, (state, { payload: { courses } }) => handleCoursesLoaded(state, courses)),
  on(loadReminders, state => handleLoadReminders(state)),
  on(remindersLoaded, (state, { payload: { reminders } }) => handleRemindersLoaded(state, reminders))
);


export function reducer(state, action) {
  return appReducer(state, action);
}
