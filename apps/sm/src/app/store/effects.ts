import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { coursesLoaded, loadCourses, loadReminders, remindersLoaded } from './actions';
import {  map, mergeMap } from 'rxjs/operators';
import { CoursesService } from '../services/courses.service';
import { RemindersService } from '../services/reminders.service';

@Injectable()
export class AppEffects {
  loadCourses$ = createEffect(() => this.actions$.pipe(
    ofType(loadCourses),
    mergeMap(() => this.coursesService.getCourses()
      .pipe(
        map(courses => coursesLoaded({ payload: { courses }})),
      ))
  ));

  loadReminders$ = createEffect(() => this.actions$.pipe(
      ofType(loadReminders),
      mergeMap(() => this.remindersService.getReminders()
      .pipe(
          map(reminders => remindersLoaded({ payload: { reminders }})),
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private coursesService: CoursesService,
              private remindersService: RemindersService) {
  }
}
