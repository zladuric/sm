import { select } from '@ngrx/store';

export const coursesSelector = select((state: any) => state.app.courses)
export const coursesLoadingSelector = select((state: any) => state.app.coursesLoading);
