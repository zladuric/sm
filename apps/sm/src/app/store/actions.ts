import { createAction, props } from '@ngrx/store';
import { Course, Reminder } from '../services/models';

export const increment = createAction('[Course] increase', props<{ payload: { id: string } }>());
export const decrement = createAction('[Course] decrease', props<{ payload: { id: string } }>());
export const flip = createAction('[Reminder] flip', props<{ payload: { id: string } }>());

export const loadCourses = createAction('[Course] load courses');
export const coursesLoaded = createAction('[Course] courses loaded', props<{ payload: { courses: Course[] }}>())

export const loadReminders = createAction('[Reminder] load reminders');
export const remindersLoaded = createAction('[Reminder] reminders loaded', props<{ payload: { reminders: Reminder[] }}>())
