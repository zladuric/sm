import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Progress } from './sidebar/progress/progress.component';
import { MatIconModule } from '@angular/material/icon';
import { Reminders } from './sidebar/reminders/reminders.component';
import { WidgetsModule } from '../widgets/widgets.module';



@NgModule({
  declarations: [SidebarComponent, Reminders, Progress],
  imports: [
    CommonModule,
    MatIconModule,
    WidgetsModule,
  ],
  exports: [SidebarComponent],
})
export class SidebarModule { }
