import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Progress } from './progress.component';

describe('ProgressComponent', () => {
  let component: Progress;
  let fixture: ComponentFixture<Progress>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Progress ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Progress);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
