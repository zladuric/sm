import { Component, OnInit } from '@angular/core';
import { Course } from '../../../services/models';
import { CoursesService } from '../../../services/courses.service';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppState } from '../../../store/reducers';
import { select, Store } from '@ngrx/store';
import { coursesLoadingSelector, coursesSelector } from '../../../store/selectors';

@Component({
  selector: 'sm-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss']
})
export class Progress {

  courses$: Observable<Course[]>;
  coursesLoading$: Observable<boolean>

  constructor(private store: Store<AppState>) {
    this.courses$ = this.store.pipe(
      coursesSelector
    );

    this.coursesLoading$ = this.store.pipe(coursesLoadingSelector);
  }

}
