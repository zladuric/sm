import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/reducers';
import { loadCourses, loadReminders } from '../../store/actions';

@Component({
  selector: 'sm-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {


  constructor(private store: Store<AppState>) {
    this.store.dispatch(loadCourses());
    this.store.dispatch(loadReminders());
  }

  ngOnInit(): void {
  }

}
