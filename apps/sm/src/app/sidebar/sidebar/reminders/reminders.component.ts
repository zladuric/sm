import { Component, OnInit } from '@angular/core';
import { RemindersService } from '../../../services/reminders.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sm-reminders',
  templateUrl: './reminders.component.html',
  styleUrls: ['./reminders.component.scss']
})
export class Reminders implements OnInit {
  reminders = []
  remindersLoading = false;

  constructor(private remindersS: RemindersService) {
  }

  ngOnInit() {
    this.remindersLoading = true;
    this.remindersS.getReminders()
      .pipe(finalize(() => this.remindersLoading = false))
      .subscribe(reminders => this.reminders = reminders);
  }
}
