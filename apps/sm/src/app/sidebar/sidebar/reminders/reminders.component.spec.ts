import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Reminders } from './reminders.component';

describe('CoursesComponent', () => {
  let component: Reminders;
  let fixture: ComponentFixture<Reminders>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Reminders ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Reminders);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
