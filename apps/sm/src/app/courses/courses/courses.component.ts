import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import { Course } from '../../services/models';
import { finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/reducers';
import { loadCourses } from '../../store/actions';

@Component({
  selector: 'sm-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courses: Course[] = [];
  loading = false;

  constructor(private coursesService: CoursesService, private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(loadCourses());
    this.loading = true;
    this.coursesService.getCourses()
      .pipe(finalize(() => this.loading = false))
      .subscribe(courses => this.courses = courses);

  }

  inc(id: string) {
    const course = this.courses.find(c => c.id === id);
    if (course.completed < 10) {
      course.completed++;
    }
  }

  dec(id: string) {
    const course = this.courses.find(c => c.id === id);
    if (course.completed > 0) {
      course.completed--;
    }
  }
}
