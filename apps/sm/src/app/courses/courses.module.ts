import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from './courses/courses.component';
import { RouterModule } from '@angular/router';
import { AppModule } from '../app.module';
import { WidgetsModule } from '../widgets/widgets.module';



@NgModule({
  declarations: [CoursesComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: CoursesComponent }]),
    WidgetsModule,
  ],
})
export class CoursesModule { }
