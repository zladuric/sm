import {
  getCompletionPercentage,
  getWelcomeBackMessage,
  getDashboardTitle,
  getLogo,
  getNavEntries,
  getNavigation,
  getSidebar,
  getSidebarAvatar,
  getSidebarName,
  getSidebarProgress,
  getSidebarReminders,
  getSidebarLogoutButton,
  getGoodTimes,
  getBadTimes, getSidebarRemindersTitle,
} from '../support/app.po';

describe('sm', () => {
  beforeEach(() => cy.visit('/'));

  it('should display the logo', () => {
    getLogo().should('be.visible')
      .and(($img) => {
        // "naturalWidth" and "naturalHeight" are set when the image loads
        // @ts-ignore
        expect($img[0].naturalWidth).to.be.greaterThan(0);
      });
  });

  it('should have the sidebar with 6 nav sections', () => {
    getNavigation().should('be.visible');
    getNavEntries().should('have.length', 6);
  });

  describe('dashboard', () => {
    it('should have the Dashboard with title and date and a search thing and Welcome back message', () => {
      getDashboardTitle().contains('Dashboard');
    });

    it('should have a welcome back section', () => {
      getWelcomeBackMessage().contains('Welcome back, Alia');
    });

    it('should have the 80% complete number', () => {
      getCompletionPercentage().contains('80%');
    });

    it('should have the good times graph', () => {
      getGoodTimes().should('be.visible');
    });

    it('should have the bad times graph', () => {
      getBadTimes().should('be.visible');
    });
  });

  describe('right sidebar', () => {
    it('should have the right sidebar', () => {
      getSidebar().should('be.visible');
      getSidebar().contains('Logout');
    });

    it('should have a logout button', () => {
      getSidebarLogoutButton().should('be.visible');
    });

    it('should have the name, avatar and the logout button', () => {
      getSidebarName().contains('Alia Randolph');
      getSidebarAvatar().should('be.visible')
        .and(($img) => {
          // "naturalWidth" and "naturalHeight" are set when the image loads
          // @ts-ignore
          expect($img[0].naturalWidth).to.be.greaterThan(0);
        });
    });

    it('should have the progress on two courses', () => {
      getSidebarProgress().should('have.length', 2);
    });

    it('should have reminders title and 3 reminders', () => {
      getSidebarRemindersTitle().contains('Reminders');
      getSidebarReminders().should('have.length', 3);
    });
  });
});
